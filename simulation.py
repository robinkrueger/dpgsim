#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Robin Krueger
robin.krueger@physik.tu-berlin.de
"""

import numpy as np
from numpy import fft
import matplotlib.pyplot as plt
from tqdm import tqdm
from PIL import Image
import io
import time
import os
import sys
import wave_propagation_toolbox as wpt
from wave_propagation_toolbox import km, m, dm, cm, mm, um, A
import multiprocessing as mp
from multiprocessing import shared_memory
from itertools import repeat

def do_sim(dic):
    #--- single simulation start ---#
    n = dic['n (bit)']
    size = dic['size (m)']
    # create real-space x-values
    x = np.linspace(-size/2, size/2, num=n)
    # creat reciprocal-space spatial frequencies
    v_x = fft.fftfreq(len(x), d=(size/n))
    # resolution real-space
    dic['resolution (m)'] = size/n
    # calculate corresponding energy
    dic['wavelength (m)'] = wpt.energy2wavelength(dic['energy (ev)'])
    # compute aperture
    dic = wpt.compute_aperture(dic)
    
    # calculate gratings
    G_1 = wpt.grating(x,dic,gratingname='1')
    G_1 = wpt.aperture(G_1,window = dic['aperture_g1 (percent)'])
    
    G_2 = wpt.grating(x,dic,gratingname='2')
    G_2 = wpt.aperture(G_2,window = dic['aperture_g2 (percent)'])    
    print(dic['aperture_g2 (percent)'])
    
    # performing the simulation
    if dic['source_type'] == 'point':
        wave = wpt.point_source(x,x_offset=dic['source_offset (m)'],
                                d=dic['d_1 (m)'],
                                wavelength=dic['wavelength (m)'])
    elif dic['source_type'] == 'parallel':
        wave = wpt.parallel_source(x)
    else:
        print('Wrong source type specified!')
        

    
    if dic['usage_g1 (bool)']:
        wave = wave*G_1
    wave = wpt.propagate(wave, x,
                         d=dic['d_2 (m)']-dic['d_1 (m)'],
                         v_x=v_x,
                         wavelength=dic['wavelength (m)'])
    
    if dic['usage_g2 (bool)']:
        wave = wave*G_2
    wave = wpt.propagate(wave, x,
                         d=dic['d_d (m)']-dic['d_2 (m)'],
                         v_x=v_x,
                         wavelength=dic['wavelength (m)'])
    result, x_detector = wpt.detector(wave,x,
                          pixel_size=dic['pixel_size (m)'],
                          result_type=dic['result_type'])
    #--- single simulation stop ---#
    return result

#%%

# if using with command promt
input_filename = sys.argv[1]

#if using manually
#input_filename = 'example.ini'

dic = wpt.parse_input(input_filename)
t1 = time.time()

dic = wpt.process_walking(dic)

dic = wpt.pre_calculate_grating_path(dic, 1)
dic = wpt.pre_calculate_grating_path(dic, 2)

# parallel version
if dic['parallel_processing (bool)'] == True:
    print('Parallel processing activated.')

    # creating parameter list for all versions
    dic_list = []
    for i_2 in range(len(dic['list_w2 (list)'])):
        dic[dic['name_w2']] = dic['list_w2 (list)'][i_2]
        for i_1 in range(len(dic['list_w1 (list)'])):
            dic[dic['name_w1']] = dic['list_w1 (list)'][i_1]
            dic_list.append(dic.copy())

    # start multiprocessing
    num_threads = mp.cpu_count()
    if num_threads > dic['max_cpu (float)']:
        num_threads = int(dic['max_cpu (float)'])
    with mp.Pool(num_threads) as p:
        result = np.array(list(tqdm(p.imap(do_sim, dic_list), unit="Lines", total=len(dic_list))))

    # re-sort output of Multiprocessing
    # case if both directions are walking
    if dic['usage_w1 (bool)'] and dic['usage_w2 (bool)']:   
        result_array = np.reshape(np.array(result),(len(dic['list_w2 (list)']),len(dic['list_w1 (list)']),result.shape[1]))
    # case if direction 1 is walking
    elif dic['usage_w1 (bool)']:
        result_array = np.reshape(np.array(result),(1,len(dic['list_w1 (list)']),result.shape[1]))
    # case if direction 2 is walking 
    elif dic['usage_w2 (bool)']:
        result_array = np.reshape(np.array(result),(len(dic['list_w2 (list)']),1,result.shape[1]))
    # case if direction no walking is done 
    elif (not dic['usage_w1 (bool)']) and (not dic['usage_w2 (bool)']):
        result_array = np.reshape(np.array(result),(1,1,result.shape[1]))

    # clean up shared memory
    if dic['calculate_path_once_g1 (bool)']:
        print('\nCleaning up shared Memory',dic['location_path_once_g1'])
        existing_shm_1 = shared_memory.SharedMemory(name=dic['location_path_once_g1'])
        existing_shm_1.close()
        existing_shm_1.unlink()
    if dic['calculate_path_once_g2 (bool)']:
        print('\nCleaning up shared Memory',dic['location_path_once_g2'])
        existing_shm_2 = shared_memory.SharedMemory(name=dic['location_path_once_g2'])
        existing_shm_2.close()
        existing_shm_2.unlink()


# non-parallel version
else:
    i2_list = []
    for i_2 in range(len(dic['list_w2 (list)'])):
        print(i_2+1,'/',len(dic['list_w2 (list)']))
        dic[dic['name_w2']] = dic['list_w2 (list)'][i_2]
        
        i1_list = []
        for i_1 in tqdm(range(len(dic['list_w1 (list)']))):
            dic[dic['name_w1']] = dic['list_w1 (list)'][i_1]
            i1_list.append(do_sim(dic))
        i2_list.append(i1_list)        
    result_array = np.array(i2_list)

# calculate the time needed for the simulation
t2 = time.time()
dic['time_start'] = time.strftime("%Y-%m-%d_%H-%M-%S",time.gmtime(t1))
dic['time_stop'] = time.strftime("%Y-%m-%d_%H-%M-%S",time.gmtime(t2))
dic['time_duration'] = time.strftime("%H-%M-%S",time.gmtime(t2-t1))

if dic['save (bool)']:
    wpt.save_data(dic, result_array, input_filename)
