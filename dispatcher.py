#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 17:39:26 2022

This scipt manages multiple simulation input files and executes the core simlation.

@author: robin krueger, robin.krueger@physik.tu-berlin.de
"""

import os
import wave_propagation_toolbox as wpt
import time

ToDo_folder = r'/home/robin/Simulation/ToDo/'

# storage space pre calculation for the already existing input files in the ToDo directory
ToDo_filenames = os.listdir(ToDo_folder)
if len(ToDo_filenames) >= 1:
    print('Estimated Storage Space:\nFile \t Space in MB')
    storage_total = 0
    for filename in ToDo_filenames:
        file_path = os.path.join(ToDo_folder+filename)
        storage = wpt.calcualte_estimated_storage_space(file_path)
        print(filename,'\t', storage)
        storage_total = storage_total + storage
    print('Total Estimated Storage-Space:', storage_total, ' MB')
 
already_waiting = False

try:
    while True:
        # get input files from the directory
        input_filenames = os.listdir(ToDo_folder)
        
        # if there is a least one file
        if len(input_filenames) >= 1:
            input_filenames.sort()
            filename = input_filenames[-1]
            
            # print status
            print('Processing File {} / {}'.format(1,len(input_filenames)))
            origin_path = os.path.join(ToDo_folder+filename)
            # perform the simulation
            os.system('python simulation.py '+origin_path)
        
            ## move the input file to the same directory as the result of the simulation
            # investigate the directory name of the last simulation
            dic = wpt.parse_input(origin_path)
            saving_dir  = dic['path']
            dirnames_done_simulations = os.listdir(saving_dir)
            dirnames_done_simulations.sort()
            dir_current = dirnames_done_simulations[-1]
            # move file to that directory    
            os.system('mv '+origin_path+' '+os.path.join(saving_dir,dir_current,filename))
            already_waiting = False
        else:
            if not already_waiting:
                print('Waiting for new input files...')
                already_waiting = True
            time.sleep(1)
except KeyboardInterrupt:
    exit