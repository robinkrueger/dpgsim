#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Robin Krueger
robin.krueger@physik.tu-berlin.de
"""

import numpy as np
import xraydb as db
import os
import scipy
import spekpy as sp
from scipy.ndimage import gaussian_filter1d

from wave_propagation_toolbox import km, m, dm, cm, mm, um, A, u, k_B
import wave_propagation_toolbox as wpt

a = 3

precalculated_spectra = {'NanoTube': [40,60],
                         'D2solid': [30,40,50,60,70],
                         'MetalJetG1': [70],
                         'MetalJetI1': [70],
                         'MetalJetI2': [70]}


def generate_spectra_spekpy(energies,target,v_acc,angle):
    """ Still in progress """
    s = sp.Spek(kvp=60.05,
                th=4,
                dk=0.1,
                targ='W')
    int_spek = s.get_spk() 
    energies_spek = np.round(s.get_k(),1)

    # extract m
    start_index = np.abs(energies_spek-np.min(energies)).argmin()
    print(energies_spek[start_index])
    stop_index = np.abs(energies_spek-np.max(energies)).argmin()
    print(energies_spek[stop_index])

    int_spek = int_spek[start_index:stop_index+1]

    return 0

def import_source_spectra(source,v_acc):
    """
    Parameters
    ----------
    source : string
        Type of the vource (NanoTube/D2solid/MetalJetG1/MetalJetI1/MetalJetI2)
    v_acc : float or int
        Acceleration voltage of the x-ray source

    Returns
    -------
    energy, intensity

    """
    try:
        data = np.genfromtxt(os.path.join('data','excillum_{}_{}kV.csv'.format(source,v_acc)), delimiter=",")
        return data[:,0], data[:,1]
    except:
        print('This acceleration Voltage is not pre-calculated!')

def transmission_coefficient_air(energies,distance,p=101325,T=293.15):
    # atomic weights
    M_N = 14.007 # u
    M_O = 15.999 # u
    M_Ar = 39.948 # u
    
    # volume part in atmosphere
    N2_percent_vol = 78.084 # % 
    O2_percent_vol = 20.942 # %
    Ar_percent_vol = 0.934 # %

    # particel in one 1m**3 air according to the law of the ideal gas
    N = p/(k_B*T)
    N_N2 = N * N2_percent_vol/100
    N_O2 = N * O2_percent_vol/100
    N_Ar = N * Ar_percent_vol/100
    
    # mass per individial element per m**3 (Density)
    rho_N2 = u * 2 * N_N2 * M_N
    rho_O2 = u * 2 * N_O2 * M_O
    rho_Ar = u * N_Ar * M_Ar
    
    # conversion to g/cm**3
    rho_N2 = rho_N2 * 1e-3
    rho_O2 = rho_O2 * 1e-3
    rho_Ar = rho_Ar * 1e-3
    
    T = np.exp(-(distance/cm)*(rho_N2*db.mu_elam('N',energies*1000)+rho_O2*db.mu_elam('O',energies*1000)+rho_Ar*db.mu_elam('Ar',energies*1000)))
    return T
    
def detector_efficiency(energies,detector):
    """ detector: EigerSi/GSENSE"""
    if detector == 'EigerSi':
        transmission = np.exp(-450e-4*db.atomic_density('Si')*db.mu_elam('Si',energies*1000))
        return 1-transmission
    elif detector == 'GSENSEmanufacturer':
        PScam_10um = np.genfromtxt('data/PScam_10um.csv', delimiter=';')
        PScam_20um = np.genfromtxt('data/PScam_20um.csv', delimiter=';')
        
        f_10um = scipy.interpolate.interp1d(PScam_10um[:,0],
                                    PScam_10um[:,1],
                                    bounds_error=False,
                                    fill_value=(PScam_10um[0,1],PScam_10um[-1,1]))
        f_20um = scipy.interpolate.interp1d(PScam_20um[:,0],
                                    PScam_20um[:,1],
                                    bounds_error=False,
                                    fill_value=(PScam_20um[0,1],PScam_20um[-1,1]))
        
        pscam_17um = f_10um(energies) + (f_20um(energies)-f_10um(energies))*7/10
        pscam_eff = scipy.interpolate.interp1d(energies,pscam_17um)
        return pscam_eff(energies)
    elif detector == 'GSENSmodel':
        """ToDo"""
        return
    else:
        print('This detector is not in the Database.')
    
def transmission_coefficient_single_grating(energies,thickness=280*2*um,material='Si'):
    transmission = np.exp(-thickness*100*db.atomic_density(material)*db.mu_elam(material,energies*1000))
    return transmission

def transmission_coefficient_dual_grating(energies,thickness=280*um,material='Si'):
    transmission = np.exp(-thickness*100*db.atomic_density(material)*db.mu_elam(material,energies*1000))
    return transmission

def import_simulation(path):
    data = np.load(os.path.join(path,'result.npy'))
    dic = wpt.parse_input(os.path.join(path,'parameters.ini'))
    return data, np.array(dic['list_w1 (list)']), np.array(dic['list_w2 (list)']), dic['size (m)']
    
def interpolate_source_spectra(energies,source,v_acc):
    orig_energies, orig_intensities = import_source_spectra(source,v_acc)
    interpolated_intesities = scipy.interpolate.interp1d(orig_energies,
                                orig_intensities,
                                bounds_error=False,
                                fill_value=(orig_intensities[0],orig_intensities[-1]))
    return interpolated_intesities(energies)
    
    
    
    