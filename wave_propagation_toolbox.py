#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Robin Krueger
robin.krueger@physik.tu-berlin.de
"""
import numpy as np
from numpy import fft
import matplotlib.pyplot as plt
from tqdm import tqdm
from PIL import Image
from matplotlib.colors import ListedColormap
import configparser
import xraydb as db
import os
from multiprocessing import shared_memory

# define units
km = 1e3
m  = 1 
dm = 1e-1
cm = 1e-2
mm = 1e-3
um = 1e-6
nm = 1e-9 
A  = 1e-10 # Angström

# define physical constants
c  = 299792458
h  = 6.62607015e-34
e  = 1.602176634e-19
u = 1.660539e-27
k_B = 1.38064852e-23

# dictionary for the units
unit_dic = {}
unit_dic['km'] = km 
unit_dic['m']  = m
unit_dic['dm'] = dm
unit_dic['cm'] = cm
unit_dic['mm'] = mm
unit_dic['um'] = um
unit_dic['nm'] = nm
unit_dic['A']  = A

def bundle_plot_adjustement_parameters(top,bottom,left,right,hspace,wspace):
    return left,bottom,right,top,hspace,wspace

def calcualte_estimated_storage_space(input_file):
    """Returns estimated Storage Space in MB"""    
    dic = parse_input(input_file)
    dic = process_walking(dic)
    
    number_of_calculations = len(dic['list_w1 (list)'])*len(dic['list_w2 (list)'])
    
    if dic['pixel_size (m)'] == 0:
        number_of_points = dic['n (bit)'] 
    else:
        number_of_points = dic['size (m)']/dic['pixel_size (m)']
    
    datapoints = number_of_calculations*number_of_points
    
    storage_space = 12000 # in bytes, start value is the size of the parameter file
    if dic['save (bool)']:
        storage_space = storage_space + datapoints*64/8 # 64 bit saving of numpy array -> 
        #print(storage_space)
        if dic['tiff (bool)']:
            storage_space = storage_space + datapoints*32/8 # 32 bit saving of tiff files
            #print(storage_space)
        
    return storage_space/1e6


def save_data(dic,array,input_filename):
    # calculate parameters for the output file
    n = dic['n (bit)']
    size = dic['size (m)']
    # resolution real-space
    dic['resolution (m)'] = size/n
    # calculate corresponding energy
    dic['wavelength (m)'] = energy2wavelength(dic['energy (ev)'])
    
    sim_directory = os.path.join(dic['path'],dic['time_start']+'_'+dic['name'].replace(' ','_'))
    os.mkdir(sim_directory)
    np.save(os.path.join(sim_directory,'result.npy'), array)
    write_config_output(input_filename, os.path.join(sim_directory, 'parameters.ini'), dic)
    
    if dic['tiff (bool)']:
        if dic['tdir'] == '1':
            direction = 1
        elif dic['tdir'] == '2':
            direction = 0

        tiff_directory = os.path.join(sim_directory,'tiff_files')
        os.mkdir(tiff_directory)
        for i in range(array.shape[direction]):
            if dic['tdir'] == '2':
                walking_variable = dic['name_w2'].split(' ')[0]
                value = dic['list_w2 (list)'][i]
                array2D =  array[i,:,:]
    
            elif dic['tdir'] == '1':
                walking_variable = dic['name_w1'].split(' ')[0]
                value = dic['list_w1 (list)'][i]
                array2D = array[:,i,:]
               
            save_to_tiff(array2D, tiff_directory,'index-{:05d}-{}-{}.tiff'.format(i,walking_variable,value))
    
    print('\nData written to: ', sim_directory)

def process_walking(dic):
    if dic['usage_w1 (bool)']:
        if dic['modus_w1'] == 'list':
            dic['list_w1 (list)'] = dic['list_w1 (list)']
        elif dic['modus_w1'] == 'range':
            dic['list_w1 (list)'] = np.arange(dic['start_w1 (unit)'],
                                              dic['stop_w1 (unit)'],
                                              dic['step_w1 (unit)'])
        else:
            print('Wrong modus for walking variable 1 was given!')
    else:
        dic['list_w1 (list)'] = [dic[dic['name_w1']]]
    
    if dic['usage_w2 (bool)']:
        if dic['modus_w2'] == 'list':
            dic['list_w2 (list)'] = dic['list_w2 (list)']
        elif dic['modus_w2'] == 'range':
            dic['list_w2 (list)'] = np.arange(dic['start_w2 (unit)'],
                                              dic['stop_w2 (unit)'],
                                              dic['step_w2 (unit)'])
        else:
            print('Wrong modus for walking variable 2 was given!')    
    else:
        dic['list_w2 (list)'] = [dic[dic['name_w2']]]
    return dic


def save_to_tiff(img, path, fname):
    """ Save image to tiff """
    tmp = Image.fromarray(img)
    tmp.save(os.path.join(path, fname))

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

def array2string(array):
    string = ''
    for entry in array:
        string += str(entry)
        string += ', '
    return string[:-2]
    
def write_config_output(input_filename, output_filename, dic_in):
    config = configparser.ConfigParser()
    config.read(input_filename)

    for section in config.sections():
        section_dic = dict(config[section])
        section_list = list(section_dic)
        for entry in section_list:
            if '(list)' in entry: 
                config[section][entry] = array2string(dic_in[entry])
            else:
                config[section][entry] = str(dic_in[entry])
            
    with open(output_filename, 'w') as configfile:
        config.write(configfile)

def parse_input(filename):
    config = configparser.ConfigParser()
    config.sections()
    config.read(filename)
    dic = {}

    for section in config.sections():
        section_dic = dict(config[section])
        section_list = list(section_dic)
        for entry in section_list:
            #print(entry, config[section][entry])
            if '(km)' in entry:
                dic[entry] = float(config[section][entry])*km
            elif '(m)' in entry:
                dic[entry] = float(config[section][entry])*m
            elif '(cm)' in entry:
                dic[entry] = float(config[section][entry])*cm
            elif '(dm)' in entry:
                dic[entry] = float(config[section][entry])*dm
            elif '(mm)' in entry:
                dic[entry] = float(config[section][entry])*mm
            elif '(um)' in entry:
                dic[entry] = float(config[section][entry])*um
            elif '(nm)' in entry:
                dic[entry] = float(config[section][entry])*nm
            elif '(A)' in entry:
                dic[entry] = float(config[section][entry])*A
            elif '(bool)' in entry:
                dic[entry] = str2bool(config[section][entry])         
            elif '(bit)' in entry:
                dic[entry] = 2**int(config[section][entry])
            elif '(ev)' in entry:
                dic[entry] = float(config[section][entry])
            elif '(float)' in entry:
                dic[entry] = float(config[section][entry])   
            elif '(percent)' in entry:
                dic[entry] = float(config[section][entry])   
            elif '(unit)' in entry:
                dic[entry] = float(config[section][entry])    
            elif '(list)' in entry:
                str_list = config[section][entry].split(',')
                dic[entry] = [float(i) for i in str_list]
            elif '(pi)' in entry:
                dic[entry] = float(config[section][entry])*np.pi
            elif '(rad)' in entry:
                if config[section][entry] == 'pi':
                    dic[entry] = np.pi
                elif  config[section][entry] == 'pi/2':
                    dic[entry] = np.pi/2
                else:
                    dic[entry] = float(config[section][entry])
            else:
                dic[entry] = config[section][entry]
                
                
    return dic

def create_colormap():
    """
    Creating colormap for phase plots. 

    Returns
    -------
    newcmp : Colormap
    """
    N = 256
    vals = np.ones((N, 4))
    vals[:, 0] = np.append(np.linspace(0, 1, int(N/2)),np.flip(np.linspace(0, 1, int(N/2))))
    vals[:, 1] = np.append(np.linspace(0, 1, int(N/2)),np.flip(np.linspace(0, 1, int(N/2))))
    vals[:, 2] = np.append(np.linspace(0, 1, int(N/2)),np.flip(np.linspace(0, 1, int(N/2))))
    newcmp = ListedColormap(vals)
    return newcmp
cm_phase = create_colormap()

def phase(z):
    """
    Calculates the phase of a complex number

    Parameters
    ----------
    z : complex float OR Array

    Returns
    -------
    complex float OR array
        Argument of the complex input.

    """
    return np.angle(z)

def plot_complex_1D(array):
    """
    Visualizes a complex 1D Array while displaying real part, imaginary part, absolute value and argument.

    Parameters
    ----------
    array : complex 1D array

    Returns
    -------
    None.

    """
    fig, ax = plt.subplots(2,2,figsize=(6,3),sharex='col')
    ax[0,0].plot(np.real(array))
    ax[0,0].set_ylabel('Real Part')
    ax[0,1].plot(np.imag(array))
    ax[0,1].set_ylabel('Imaginary Part')
    ax[0,1].yaxis.set_label_position('right')
    ax[0,1].yaxis.set_ticks_position('right')
    ax[1,0].plot(np.abs(array))
    ax[1,0].set_ylabel('Absolute Value')
    ax[1,0].set_xlabel('Index')
    ax[1,1].plot(phase(array))
    ax[1,1].set_ylabel('Argument')
    ax[1,1].set_xlabel('Index')
    ax[1,1].yaxis.set_label_position('right')
    ax[1,1].yaxis.set_ticks_position('right')
    plt.subplots_adjust(hspace=0.1,wspace=0.1)
    
def plot_complex_2D(array,ds=[0],x=[0]):
    """
    Visualizes a complex 1D Array while displaying real part, imaginary part, absolute value and argument.

    Parameters
    ----------
    array : complex 1D array
        Input.
    ds : 1D array, optional
        Positions in horizontal direction. The default is [0].
    x : 1D array, optional
        Positions in vertical direction.The default is [0].

    Returns
    -------
    None.

    """
    fig, ax = plt.subplots(2,figsize=(10,6),sharex='col')
    if len(ds) == 1:
        ds = np.arange(array.shape[1])
    else:
        ax[1].set_xlabel('Position in m')
    if len(x) == 1:
        x = np.arange(array.shape[0])
    else:
        ax[0].set_ylabel('Position in m')
        ax[1].set_ylabel('Position in m')
        
    im0 = ax[0].imshow(phase(array),aspect="auto",cmap = cm_phase, extent = [np.min(ds), np.max(ds), np.min(x) , np.max(x)])
    plt.colorbar(im0, ax=ax[0])
    ax[0].set_title('Phase')
    im1 = ax[1].imshow(np.abs(array)**2,aspect="auto", extent = [np.min(ds), np.max(ds), np.min(x) , np.max(x)])
    plt.colorbar(im1, ax=ax[1])
    ax[1].set_title('Intensity')

def plot_int_2D(array,ds=[0],x=[0]):
    """Plot Phase and Intensity of an 2D complex array"""
    fig, ax = plt.subplots(1,figsize=(10,6),sharex='col')
    if len(ds) == 1:
        ds = np.arange(array.shape[1])
    else:
        ax.set_xlabel('Position in m')
    if len(x) == 1:
        x = np.arange(array.shape[0])
    else:
        ax.set_ylabel('Position in m')
        ax.set_ylabel('Position in m')
    im1 = ax.imshow(np.abs(array)**2,aspect="auto", extent = [np.min(ds), np.max(ds), np.min(x) , np.max(x)])
    plt.colorbar(im1, ax=ax)
    ax.set_title('Intensity')

def propagate_1D(input_wave, distance, steps, n, x, v_x, wavelength):
    result = np.empty((n,steps), dtype=complex)
    d_s = np.linspace(0,distance,steps)
    for i in tqdm(range(len(d_s)),unit='steps'):
        H = np.exp(1j*2*np.pi*d_s[i]*np.emath.sqrt((1/wavelength**2)-v_x**2))
        result[:,i] = fft.fft(H*fft.fft(input_wave))
    return result, d_s


def binArray(data, axis, binstep, binsize, func=np.nanmean):
    data = np.array(data)
    dims = np.array(data.shape)
    argdims = np.arange(data.ndim)
    argdims[0], argdims[axis]= argdims[axis], argdims[0]
    data = data.transpose(argdims)
    data = [func(np.take(data,np.arange(int(i*binstep),int(i*binstep+binsize)),0),0) for i in np.arange(dims[axis]//binstep)]
    data = np.array(data).transpose(argdims)
    return data


def propagate_1D_binning(input_wave, distance, steps, n, x, v_x, wavelength,binning):
    result = np.empty((int(n/binning),steps), dtype=complex)
    d_s = np.linspace(0,distance,steps)
    for i in tqdm(range(len(d_s)),unit='steps'):
        H = np.exp(1j*2*np.pi*d_s[i]*np.emath.sqrt((1/wavelength**2)-v_x**2))
        result[:,i] = binArray(np.abs(fft.fft(H*fft.fft(input_wave))),0,binning,binning)
    return result, d_s


def energy2wavelength(energy):
    return h*c/(energy*e)


def wavelength2energy(wavelength):
    return h*c/(wavelength*e)
    
def phase_shift(element, height, energy, wrap=True):
    wavelength = energy2wavelength(energy)
    delta, _ ,_  = db.xray_delta_beta(element, db.atomic_density(element), energy)
    phi = delta*2*np.pi*height/wavelength
    if wrap==True:
        return phi%(2*np.pi)
    else:
        return phi

def transmission_coefficient(element, height, energy, variant='beta'):
    if variant=='beta':
        _, beta, _  = db.xray_delta_beta(element, db.atomic_density(element), energy)
        T = np.exp(-2*np.pi*height*beta/energy2wavelength(energy))
    elif variant=='mulen':
        _, _, mulen  = db.xray_delta_beta(element, db.atomic_density(element), energy)
        T = np.sqrt(np.exp(-height/(mulen*cm)))
    elif variant =='mu_elam':
        mu = db.mu_chantler(element,energy)
        T = np.sqrt(np.exp(-height*db.atomic_density(element)*mu/cm))
    elif variant =='mu_chantler':
        mu = db.mu_elam(element,energy)
        T = np.sqrt(np.exp(-height*db.atomic_density(element)*mu/cm))
    else:
        print('Specified wrong variant to calculate the transmission coefficient!')
    return T

def propagate(input_wave, x, d, v_x, wavelength):
    H = np.exp(1j*2*np.pi*d*np.emath.sqrt((1/wavelength**2)-v_x**2))
    return fft.fft(H*fft.fft(input_wave))

def grating(x, dic, gratingname):
    # extract the information for the two grating cases
    spacing     = dic_key(dic,'spacing_g$ (m)',gratingname)
    duty_cycle  = dic_key(dic,'duty_cycle_g$ (float)',gratingname) 
    shift       = dic_key(dic,'shift_g$ (m)',gratingname)
    material    = dic_key(dic,'material_g$',gratingname)   
    height_base = dic_key(dic,'height_base_g$ (m)',gratingname)
    height_step = dic_key(dic,'height_step_g$ (m)',gratingname)
    source_offset = dic_key(dic,'source_offset (m)',gratingname) 
    iterations  = dic_key(dic,'iterations_path_calculation_g$ (float)',gratingname) 
    calculate_path_once = dic_key(dic,'calculate_path_once_g$ (bool)',gratingname)
    location_path_once = dic_key(dic,'location_path_once_g$',gratingname)
    distance = dic_key(dic,'d_$ (m)',gratingname)
    mode = dic_key(dic,'path_calculation_g$',gratingname)
    phase_usage = dic_key(dic,'phase_usage_g$ (bool)',gratingname)
    absorption_usage = dic_key(dic,'absorption_usage_g$ (bool)',gratingname)
    
    if calculate_path_once:
        #path_length = np.load(location_path_once+'.npy')
        existing_shm = shared_memory.SharedMemory(name=location_path_once)
        path_length = np.ndarray(x.shape, dtype=x.dtype, buffer=existing_shm.buf)
    else:
        path_length = calculate_grating_path(x,
                                             d=distance,
                                             n_int=iterations,
                                             h_l=height_step,
                                             h_b=height_base,
                                             spacing=spacing,
                                             duty_cycle=duty_cycle,
                                             shift=shift,
                                             source_offset=source_offset,
                                             mode = mode)
    if absorption_usage:
        T = transmission_coefficient(material, path_length, dic['energy (ev)'])
    else:
        T = 0*path_length + 1
        
    if phase_usage:
        Phi = phase_shift(material, path_length, dic['energy (ev)'], wrap=True)
    else:
        Phi = 0*path_length + 0
        
    return T*np.exp(1j*Phi)

        
def calculate_grating_path(x,d,n_int,h_l,h_b,spacing, duty_cycle, shift, source_offset, mode):
    if mode == 'calculated_curved':
        # calculate path length threw base of the grating
        s_base = h_b * np.sqrt((x+source_offset)**2/d**2+1)          # vector
    
        z_start = h_b                                # scalar
        z_stop  = h_b+h_l                            # scalar
        x_start = (x+source_offset) * (1+h_b/d) - source_offset                      # vector supstracting the offset is new!!
        x_stop  = (x+source_offset) * (1+ (h_b+h_l)/d) - source_offset               # vector supstracting the offset is new!!
        delta_x_total = x_stop-x_start               # vector
        delta_z_total = z_stop-z_start               # scalar
        delta_x = delta_x_total/n_int                # vector
        #delta_z = delta_z_total/n_int                # scalar
    
        total_distance =  np.sqrt(delta_x_total**2+delta_z_total**2) # vector
    
        indizes = np.arange(n_int)
        result = np.zeros(x.shape[0])
        for idx in indizes:
            positions_x  = x_start + idx * delta_x  # vector
            #positions_z = z_start + idx * delta_z   # scalar
    
            result += np.mod(positions_x+shift,spacing)/spacing-duty_cycle < 0
    
        path_length = total_distance * result/n_int + s_base
    elif mode == 'calculated_parallel':
        path_length = (np.mod(x+shift,spacing)/spacing-duty_cycle < 0)*h_l+h_b
    elif mode == 'calculated_curved_fast':
        # not working yet!!
        # calculate path length threw base of the grating
        s_base = h_b * np.sqrt((x+source_offset)**2/d**2+1)          # vector
        s_base = 0
        
        # calculate grating lamelle area
        z_start = h_b                                # scalar
        z_stop  = h_b+h_l                            # scalar
        x_start = (x+source_offset) * (1+h_b/d) - source_offset                      # vector supstracting the offset is new!!
        x_stop  = (x+source_offset) * (1+ (h_b+h_l)/d) - source_offset               # vector supstracting the offset is new!!
        delta_x_total = x_stop-x_start               # vector
        delta_z_total = z_stop-z_start               # scalar        
        
        total_distance =  np.sqrt(delta_x_total**2+delta_z_total**2) # vector
        
        phi1 = np.mod(x_start+shift,spacing)/spacing
        phi2 = np.mod(x_stop+shift,spacing)/spacing
        
        f_1 = (-spacing*phi1+duty_cycle*spacing)*np.heaviside(duty_cycle-phi1,0)
        f_2 = -(-spacing*phi2+duty_cycle*spacing)*np.heaviside(duty_cycle-phi2,0) + spacing*duty_cycle
        n = np.floor((delta_x_total-phi2*spacing)/spacing)
        s_projected = np.abs(f_1) + n*duty_cycle + np.abs(f_2)
        
        s_lamella = np.abs(s_projected)*total_distance/np.abs(delta_x_total)
        
        path_length = s_base + s_lamella
    elif mode == 'calculated_curved_x_fast':
        # not working yet!!
        # calculate path length threw base of the grating
        s_base = h_b * np.sqrt((x+source_offset)**2/d**2+1)          # vector
        
        # calculate grating lamelle area
        z_start = h_b                                # scalar
        z_stop  = h_b+h_l                            # scalar
        x_start = (x+source_offset) * (1+h_b/d) - source_offset                      # vector supstracting the offset is new!!
        x_stop  = (x+source_offset) * (1+ (h_b+h_l)/d) - source_offset               # vector supstracting the offset is new!!
        delta_x_total = x_stop-x_start               # vector
        delta_z_total = z_stop-z_start               # scalar        
        
        total_distance =  np.sqrt(delta_x_total**2+delta_z_total**2) # vector
        
        cumsum = np.cumsum(np.mod(x+shift,spacing)/spacing-duty_cycle < 0)

        x_start_index = ((x_start/(2*np.max(x))+0.5)*len(x_start)).astype(int)
        x_start_index[np.where(x_start_index<0)] = 0
        x_start_index[np.where(x_start_index>(len(x_start)-1))] = (len(x_start)-1)

        x_stop_index = ((x_stop/(2*np.max(x))+0.5)*len(x_stop)).astype(int)
        x_stop_index[np.where(x_stop_index<0)] = 0
        x_stop_index[np.where(x_stop_index>(len(x_stop)-1))] = (len(x_stop)-1)

        with np.errstate(divide='ignore', invalid='ignore'):
            s_projected = np.abs(cumsum[x_stop_index]-cumsum[x_start_index])
        
        with np.errstate(divide='ignore', invalid='ignore'):
            s_lamella = s_projected*total_distance/np.abs(delta_x_total)  #*(x_stop_index-x_start_index)

        
        path_length =  s_lamella + s_base      
        #path_length = cumsum      
    else:
        print('wrong modus for path length calculation specified')
    return path_length

def pre_calculate_grating_path(dic, gratingname):
    # extract the information for the two grating cases
    spacing     = dic_key(dic,'spacing_g$ (m)',gratingname)
    duty_cycle  = dic_key(dic,'duty_cycle_g$ (float)',gratingname) 
    shift       = dic_key(dic,'shift_g$ (m)',gratingname)
    height_step = dic_key(dic,'height_step_g$ (m)',gratingname) 
    height_base = dic_key(dic,'height_base_g$ (m)',gratingname)
    source_offset = dic_key(dic,'source_offset (m)',gratingname) 
    iterations  = dic_key(dic,'iterations_path_calculation_g$ (float)',gratingname) 
    calculate_path_once = dic_key(dic,'calculate_path_once_g$ (bool)',gratingname)
    distance = dic_key(dic,'d_$ (m)',gratingname)
    mode = dic_key(dic,'path_calculation_g$',gratingname)
    
    if calculate_path_once:
        print('Pre-calculating path threw grating',gratingname,'...')
        size = dic['size (m)']
        # create real-space x-values
        n = dic['n (bit)']
        x = np.linspace(-size/2, size/2, num=n)
        path_length = calculate_grating_path(x,
                                             d=distance,
                                             n_int=iterations,
                                             h_l=height_step,
                                             h_b=height_base,
                                             spacing=spacing,
                                             duty_cycle=duty_cycle,
                                             shift=shift,
                                             source_offset=source_offset,
                                             mode = mode)
        #np.save(location_path_once+'.npy',path_length)
        # create a shared memory regarding https://docs.python.org/3/library/multiprocessing.shared_memory.html
        shm = shared_memory.SharedMemory(create=True, size=path_length.nbytes)
        b = np.ndarray(path_length.shape, dtype=path_length.dtype, buffer=shm.buf)
        b[:] = path_length[:]
        print('Done\n','Created shared Memory ',shm.name)
        dic['location_path_once_g$'.replace('$',str(gratingname))] = shm.name
    return dic

def dic_key(dic,key,insertation,placeholder='$'):
    return dic[key.replace(placeholder, str(insertation))]

def parallel_source(x):
    return np.zeros(len(x), dtype=complex)*0+1

def point_source(x,x_offset,d,wavelength):
    x_0 = x[int(len(x)/2)]+x_offset
    r = np.emath.sqrt((x-x_0)**2+d**2)
    # return np.exp(1j*2*np.pi*r/wavelength)*wavelength/(2*np.pi*r) # old 
    return np.exp(1j*2*np.pi*r/wavelength)*1/(2*np.pi*r) # new cahnge that ! No 2 pi!


def aperture(input_wave,window):
    # recalculation from percne to relative number
    window = window/100
    passed_wave = np.copy(input_wave)
    passed_wave[0:int(len(passed_wave)*(1-window)/2)] = 0
    passed_wave[-int(len(passed_wave)*(1-window)/2):] = 0
    return passed_wave
   

def detector(input_wave,x,pixel_size,result_type='int'):
    if result_type == 'int':
        wave = np.abs(input_wave)**2
    elif result_type == 'phase':
        wave = np.angle(input_wave)
    elif result_type == 'abs':
        wave = np.abs(input_wave)
    elif result_type == 'real':
        wave = np.real(input_wave)
    elif result_type == 'imag':
        wave = np.imag(input_wave)
    elif result_type == 'complex':
        wave = input_wave
    else:
        wave = input_wave 
    

    if pixel_size == 0:
        return wave, x
    else:
        size = np.max(x)*2
        binsize = int(pixel_size*len(input_wave)/size)
        binstep = binsize
        result = [np.nanmean(np.take(wave,np.arange(int(i*binstep),int(i*binstep+binsize)))) for i in np.arange(len(input_wave)//binstep)]
        x_detector = np.linspace(-size/2, size/2, num=len(input_wave)//binstep)        
        return result, x_detector
    
    
def compute_aperture(dic):
    if dic['aperture_auto_g1 (bool)']:
        dic['aperture_g1 (percent)'] = 0.98*100*dic['d_1 (m)']/dic['d_d (m)']
    if dic['aperture_auto_g2 (bool)']:
        dic['aperture_g2 (percent)'] = 0.98*100*dic['d_2 (m)']/dic['d_d (m)']
    return dic


def period_function(d_1,d_2,d_D,g_1,g_2):
    return d_D/(np.abs(d_1/g_1-d_2/g_2))

def min_bit(x_size,l,o_A_rel,d):
    return -np.log2((1/x_size)*np.sqrt((l/2+np.sqrt((o_A_rel**2*x_size**2)/4+d**2))**2-d**2)-o_A_rel/2)